package spf.show is

   procedure Exec( pwdfile : string ;
                   password : aliased string ;
                   ctx : string ;
                   uname : string ;
                   keepOption : boolean := false ;
                   clipOption : boolean := false ) ;

end spf.show;
