with Ada.Text_Io; use Ada.Text_Io;

with GNAT.OS_Lib ;
with sf.reader ;
with numbers ;

package body spf.show is

   procedure Exec( pwdfile : string ;
                   password : aliased string ;
                   ctx : string ;
                   uname : string ;
                   keepOption : boolean := false ;
                   clipOption : boolean := false ) is
      spwdf : sf.SecureFile_Type ;
      pwdfn : string := "PPMSHOW" & numbers.Generate & ".tmp" ;
      
      allpwds : Pwd_Pkg.Map ;
      procedure show( cursor : Pwd_Pkg.Cursor ) is
         itemkey : PwdContext := Pwd_Pkg.Key(cursor) ;
         itemval : Pwd := Pwd_Pkg.Element( cursor ) ;
      begin
         if Verbose
         then
            Put("Searching "); Reveal(itemkey,itemval);
         end if ;
         if ctx = "."
         then
            if uname = "."
            then
               Reveal(itemkey,itemval);
            elsif uname = itemkey.username
            then
               Reveal(itemkey,itemval);
            end if ;
         elsif ctx = itemkey.site then
             if uname = "."
            then
               Reveal(itemkey,itemval);
            elsif uname = itemkey.username
            then
               Reveal(itemkey,itemval);
            end if ;          
         end if ;
      end show ;
      
   begin
      if Verbose
      then
         Put("Searching for context "); Put(ctx) ; Put(" ");
         Put("Username "); Put(uname);
         New_line ;
      end if ;
      
      sf.reader.Open(spwdf, pwdfile , password );
      if Verbose
      then
         Put("Extracting to "); Put(pwdfn); New_Line ;
      end if ;
      sf.reader.Copy(spwdf, pwdfn) ; 
      sf.reader.Close(spwdf) ;
      allpwds := Load(pwdfn,not keepOption);  
      Pwd_Pkg.Iterate( allpwds , show'access ) ;
   end Exec;
   
end spf.show;
