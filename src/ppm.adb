with Ada.Text_Io; use Ada.TExt_Io ;
with GNAT.Strings ; use GNAT.Strings ;
with GNAT.Command_Line ;
with ada.strings.unbounded ; use ada.strings.Unbounded ;

with ppmcli ; use ppmcli ;
with spf ;
with spf.create ;
with spf.show ;
with spf.set ;
with spf.reset ;

with passwords ;
with sf.creator ;
with sf.reader ;
with ppmtest;

procedure Ppm is
begin
   --  Insert code here.
   begin
      ppmcli.ProcessCommandLine ;
   exception
      when GNAT.Command_Line.Exit_From_Command_Line =>
         return ;
   end ;

   spf.Verbose := ppmcli.Verbose ;
   sf.Verbose := ppmcli.verbose ;

   case ppmcli.command is
      when ppmcli.test =>
           ppmtest ;
      when ppmcli.create =>
         declare
            pwd : aliased string := ppmcli.Query("Password ");
         begin
            spf.create.Exec( ppmcli.PwdFile.all , ppmcli.forceOption , pwd );
         end ;
      when ppmcli.show =>
         declare
            cmd : string := ppmcli.GetNextArgument ;
            ctx : string := ppmcli.GetNextArgument ;
            uname : string := ppmcli.GetNextArgument ;
            pwd : aliased string := ppmcli.Query("Password ");
         begin
            spf.Show.Exec( ppmcli.PwdFile.all , pwd , ctx , uname ,
                           ppmcli.keepOption , ppmcli.clipOption );
         end ;
      when ppmcli.set =>
         if ppmcli.wordlistFile = null
         then
            if not ppmcli.builtinOption
            then
               Put_Line("Please specify a wordlist file or specify builtin");
               return ;
            end if ;
         end if ;
         if ppmcli.builtinOption
         then
            spf.Set.SetWordList( ppmcli.wordlistFile.all ,
                              ppmcli.MaxWordLength ,
                              ppmcli.NumSegments ,
                              ppmcli.separator.all , true );
         else
            spf.Set.SetWordList( ppmcli.wordlistFile.all ,
                              ppmcli.MaxWordLength ,
                              ppmcli.NumSegments ,
                              ppmcli.separator.all , false );
         end if;

         declare
            cmd : string := ppmcli.GetNextArgument ;
            ctx : string := ppmcli.GetNextArgument ;
            uname : string := ppmcli.GetNextArgument ;
            pwd : aliased string := ppmcli.Query("Password ");
            ctxpwd : ada.strings.unbounded.Unbounded_String := Null_Unbounded_String ;
            --ctxpwd : string := ppmcli.Query( "Password for " & uname & " " );
         begin
            if not ppmcli.generateOption
            then
               ctxpwd := to_unbounded_string( ppmcli.Query( "Password for " & uname & " " ));
            end if ;

            spf.Set.Exec( ppmcli.PwdFile.all ,
                          pwd ,
                          ctx ,
                          uname ,
                          to_string(ctxpwd) ,
                          overrideOption => ppmcli.overrideOption ,
                          generateOption => ppmcli.generateOption ,
                          keepOption => ppmcli.keepOption );
         end ;
      when ppmcli.reset =>
         declare
            cmd : string := ppmcli.GetNextArgument ;
            newname : string := ppmcli.GetNextArgument ;
            pwd : aliased string := ppmcli.Query("Password ");
            newpwd : aliased string := ppmcli.Query("New Password ");
         begin
            spf.reset.Exec( ppmcli.PwdFile.all , pwd , newname , newpwd ,
                           ppmcli.forceOption, ppmcli.keepOption );
         end ;
      when others =>
         Put_Line("Unrecognized command");
   end case ;
end Ppm;
