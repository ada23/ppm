with Ada.strings.Unbounded ; use ada.strings.unbounded ;
with Ada.Text_IO ; use Ada.Text_IO ;
with Ada.Directories ;

with GNAT.os_lib ;
with Ada.Exceptions ;


with sf.reader ;
with sf.creator ;

with words ;
with words_str ; 
with passwords ;
with numbers ;

package body spf.set is

   wordlist : words.CandidateWords_Type ;
   sep : unbounded_string := Null_Unbounded_String ;
   segs : Integer ;
   procedure SetWordList( wordlistFile : string ;
                          MaxWordLength : integer := 12;
                          NumSegments : integer := 1 ;
                          separator : string := "()" ;
                          builtinOption : boolean ) is
   begin 
      if builtinOption
      then
         wordlist := words.Initialize( words_str.words , ',' , MaxWordLength);
      else
         if wordlistfile'length > 1
         then   
            wordlist := words.Initialize( wordlistfile , MaxWordLength );
         end if ;       
      end if ;
      
      segs := NumSegments ;
      sep := To_Unbounded_String(separator);
      if Verbose
      then
         Put("Initialized with wordlist file "); Put_Line(wordlistFile) ;
      end if ;
   end SetWordList ;
    
   procedure Exec( PwdFile : string ;
                   password : aliased string ;
                   ctx : string ;
                   uname : string ;
                   newpwd : string := "?" ;
                   overrideOption : boolean := false ;
                   generateOption : boolean := true ;
                   keepOption : boolean := false ) is
      
      spwdf : sf.SecureFile_Type ;
      --pwdfn : string := passwords.Generate(wordlist,1,".") & ".tmp" ;
      pwdfn : string := "SET" & numbers.Generate & ".tmp" ;
      allpwds : Pwd_Pkg.Map ;
      
      item : PwdContext ;
      itemptr : Pwd_Pkg.cursor ;
      itempwd : Pwd ;
      success : boolean ;
      Except_ID : Ada.Exceptions.EXCEPTION_OCCURRENCE;
      use Pwd_Pkg ;
   begin
      if Verbose
      then
         Put("Set Option. Password file "); Put_Line(pwdfile);
      end if ;
      
      sf.reader.Open(spwdf, pwdfile , password );
      begin
         if Verbose
         then
            Put("Created temp file "); Put_Line( pwdfn ) ;
         end if ;
      exception
         when others => Put_Line("Exception creating temp file");
            raise ;
      end ;
      sf.reader.Copy(spwdf, pwdfn) ; 
      sf.reader.Close(spwdf) ;
      allpwds := Load(pwdfn,not keepOption);  
      
      item.site := to_unbounded_string(ctx) ;
      item.username := to_unbounded_string(uname) ;
      
      if verbose
      then   
         Put("Trying to find "); Put(to_string(item.site)); Put(" "); Put(to_string(item.username)); New_Line ;
      end if ;
      
      itemptr := Pwd_Pkg.Find(allpwds,item);
      if itemptr = Pwd_Pkg.No_Element
      then
         if Verbose
         then
            put_line("Old entry not found");
         end if ;
         if generateOption
         then
            itempwd.password := To_Unbounded_String(passwords.Generate(wordlist,segs,to_string(sep)));        
            if Verbose
            then
               Put("New password generated ");
               Put_Line(to_string(itempwd.password));
            end if ;
         else
            Put("Using the password "); Put_Line(newpwd);
            itempwd.password := To_Unbounded_String(newpwd) ;
         end if ;
         Pwd_Pkg.Insert(allpwds , item , itempwd );
      else
         if overrideOption
         then
            if generateOption
            then
               itempwd.password := To_Unbounded_String(passwords.Generate(wordlist,segs,to_string(sep)));        
            else
               itempwd.password := To_Unbounded_String(newpwd) ;
            end if ;
            Pwd_Pkg.Replace_Element(allpwds,itemptr,itempwd);
         else
            Put_Line("Existing credential");
            raise EXISTING_CREDENTIAL ;
         end if ;
      end if ;
      begin 
         Save(allpwds,pwdfn);
      exception
         when others =>
            Put_Line("Exception saving passwords");
      end ;
      
      sf.creator.Create(spwdf, pwdfile , password);
      sf.creator.Copy(spwdf, pwdfn );
      sf.creator.Close(spwdf) ;
      if not keepOption
      then
         Ada.Directories.Delete_File (pwdfn);
      end if ;
   exception
      when others =>
         Put_Line(Ada.Exceptions.Exception_Information(Except_ID));
   end Exec ;
   

end spf.set;
