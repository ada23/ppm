package spf.reset is

   procedure Exec( PwdFile : string ;
                   password : aliased string ;
                   newname : string ;
                   newpassword : aliased string ;
                   forceOption : boolean ;
                  keepOption : boolean ) ;

end spf.reset;
