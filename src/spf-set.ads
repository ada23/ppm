package spf.set is
   
   NOWORDLIST_FILE : exception ;
   procedure SetWordList( wordlistFile : string ;
                          MaxWordLength : integer := 12;
                          NumSegments : integer := 1 ;
                          separator : string := "()" ;
                          builtinOption : boolean );
   procedure Exec( PwdFile : string ;
                   password : aliased string ;
                   ctx : string ;
                   uname : string ;
                   newpwd : string := "?" ;
                   overrideOption : boolean := false ;
                   generateOption : boolean := true ;
                   keepOption : boolean := false );

end spf.set;
