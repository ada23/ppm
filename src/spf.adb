with Ada.Directories ;
with Ada.Text_IO; use Ada.Text_IO ;
with GNAT.AWK ;

package body spf is
   use Ada.Strings.Unbounded ;
   function "<" (Left, Right : PwdContext) return Boolean is
   begin
      if Left.site < right.site
      then
         return true ;
      end if ;
      if left.site > right.site
      then
         return false ;
      end if ;
      return left.username < right.username ;
   end "<" ;
   
   function "=" (Left, Right : Pwd) return Boolean is
   begin
      return Left.password = Right.password ;
   end "=" ;
   function  Load(filename : string;
                  remove : boolean := false ) return pwd_pkg.Map is
      use GNAT ;
      use Ada.Strings.Unbounded ;
      result : pwd_pkg.Map ;
      inpfile : file_type ;
      pwdsession : AWK.Session_Type;
   begin
      if Verbose
      then
        Put_Line("Loading from " & filename );
      end if ;

      AWK.Set_Current (pwdsession);
      AWK.Open (Separators => ";",
                Filename   => filename );
    
      while not AWK.End_Of_File 
      loop
         AWK.Get_Line;
         declare
            key : PwdContext ;
            value : Pwd ;
            site : string := AWK.field(1);
            username : string := AWK.field(2);
            pwdstr : string := AWK.field(3);
         begin
            key.site := to_unbounded_string( site ) ;
            key.username := To_Unbounded_String( username ) ;
            value.password := to_unbounded_string( pwdstr ) ;
            Pwd_Pkg.Insert( result , key , value ) ;
            if Verbose
            then
               Put("Site : "); Put( site ) ;
               Put(" username : "); Put( username ) ;
               Put(" password : "); Put(pwdstr);
               New_Line ;
            end if ;
         exception
            when others =>
               Put_Line("Exception adding a pwd ");              
         end ;         
      end loop ;
      AWK.Close (pwdsession);
      if remove
      then
         Ada.Directories.Delete_File(filename) ;
         pragma Debug( Put_Line("Removing file " & filename)  ) ;
      end if ;
      return result ;
   end Load ;
   
   procedure Save(pkg : pwd_pkg.Map ; filename : string) is
      outfile : file_type ;
      procedure Save(Position : Pwd_Pkg.Cursor) is
         ctx : PwdContext := Pwd_pkg.Key( position ) ;
         value : Pwd := pwd_pkg.Element(position) ;
      begin
         if Verbose
         then   
             Put("Key   : "); Put("Site "); Put( to_string(ctx.site) ); Put(" "); Put_Line( to_string(ctx.username));
             Put("Value : "); Put_Line( to_string(value.password) );
         end if ;
         
         Put(outfile,to_string(ctx.site)); Put(outfile,";");
         Put(outfile,to_string(ctx.username)); Put(outfile,";");
         Put(outfile,to_string(value.password)); New_Line(outfile);
      end Save;   
   begin
      Create(outfile,out_file,filename);
      Pwd_Pkg.Iterate( pkg , Save'access ) ;
      Close(outfile);
   end Save ;
   procedure SetPassword( pkg : in out pwd_pkg.Map ;
                  sitename : string ;
                  username : string ;
                  password : string ;
                  override : boolean := false ) is
      key : PwdContext ;
      val : Pwd ;
      valcursor : Pwd_Pkg.Cursor ;
      use Pwd_Pkg ;
   begin
      key.site := To_Unbounded_String(sitename) ;
      key.username := to_unbounded_string(username) ;
      valcursor := Pwd_Pkg.Find(pkg,key);
      if valcursor = Pwd_Pkg.No_Element
      then
         val.password := To_Unbounded_String( password );
         Pwd_Pkg.Insert( pkg , key , val );
      else
         if override
         then
            val.password := To_Unbounded_String( password );
            Pwd_Pkg.Replace_Element(pkg,valcursor,val) ;
         else
            raise EXISTING_CREDENTIAL ;
         end if;
         
      end if ;
      
   end SetPassword ;
   
   function Get( pkg : pwd_pkg.Map ;
                 sitename : string ;
                 username : string ) return string is
      -- result : Pwd ;
      key : PwdContext ;
      resultcursor : Pwd_Pkg.cursor ;
      use Pwd_Pkg ;
   begin
      key.site := to_unbounded_string(sitename) ;
      key.username := to_unbounded_string(username) ;
      resultcursor := Pwd_Pkg.Find( pkg , key );
      if resultcursor = Pwd_Pkg.No_Element
      then
         raise UNKNOWN_CREDENTIAL ;
      end if ;
      return to_string(pwd_pkg.Element(resultcursor).password) ;
   end Get ;
   
   procedure Reveal( itemkey : PwdContext ; itemval : Pwd ) is
   begin
      Put("Context : "); Put(to_string(itemkey.site)) ; Put(" , ");
      Put("Username : "); Put(to_string(itemkey.username)); Put( " , " ) ;
      Put("Password : "); Put(to_string(itemval.password)); New_Line ;
   end Reveal ;
   
end spf;
