with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Directories ;

with GNAT.OS_Lib ;
with GNAT.Strings ;

with numbers ;
with sf.creator ;

package body spf.create is

   
   procedure Exec( filename : string ;
                   forceOption : boolean := false ;
                   password : aliased string ) is
      result : sf.SecureFile_Type ;
      newfilename : string := "PPM" & numbers.Generate & ".tmp" ;
      
      initfile : File_Type ;    

   begin
      if Verbose
      then
         Put("Creating "); Put(filename); New_Line ;
      end if ;
      if GNAT.OS_Lib.Is_Regular_File(filename)
      then
         if not forceOption
         then
            if Verbose
            then
               Put_Line("Existing file. raising exception");
            end if ;
            raise EXISTING_FILE ;
         end if ;
      end if ;
      sf.creator.Create(result , filename , password ) ;
      if Verbose
      then
         Put("Temporary File "); Put(newfilename); New_Line ;
      end if ;
      Ada.Text_Io.Create(initfile,Out_File,newfilename) ;
      Put(initfile,"ctx"); Put(initfile,SEPARATOR);
      Put(initfile,"uname"); Put(initfile,SEPARATOR);
      Put(initfile,"pwd"); Put(initfile,SEPARATOR) ; 
      New_Line(initfile);
      Close(initfile) ;
      sf.creator.Copy(result,newfilename);         

      Ada.Directories.Delete_File(newfilename);
      sf.creator.Close(result) ;
   end Exec ;
   
end spf.create;
