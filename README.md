# ppm Personal Password Manager

## create a new password database
```
$ ../obj/ppm create --help
Usage: ppm.exe create a new personal password database

 -v, --verbose           be verbose
 -p, --password-file=ARG Password File
 -k, --keep              keep intermediate files
 -f, --force             force the creation. erase existing file
```

### Example
```
$ ../obj/ppm create -p=newpwd.ppm -f
Password myname
```

## Generate a password and save
```
$ ../obj/ppm set --help
Usage: ppm.exe set the credentials for a new context(site)

 -v, --verbose                 be verbose
 -p, --password-file=ARG       Password File
 -k, --keep                    keep intermediate files
 -f, --word-list-file-name=ARG Word list file name
 -t, --separator=ARG           Separator
 -g, --generate                generate a password
 -b, --builtin-wordlist        use builtin wordlist for generation
 -o, --override                override the existing password
 -s, --segments=ARG            Number of Segments
 -m, --max-word-length=ARG     Maximum length of words

```
### Example
```
$ ../obj/ppm set -p=newpwd.ppm -g -b -f hotmail myname
Password myname
$ ../obj/ppm show -p=newpwd.ppm hotmail .
Password myname
Context : hotmail , Username : myname , Password : pylorus10396Anole18213
```

## Create a new database with the same contents
```
$ ../obj/ppm reset --help
Usage: ppm.exe reset the credentials db. create a new one with a different password
 -v, --verbose           be verbose
 -p, --password-file=ARG Password File
 -k, --keep              keep intermediate files
 -f, --force             force the creation. erase existing file
```
### Example
```
$ ../obj/ppm reset -p=newpwd.ppm newpwdo.ppm
Password myname
New Password mynewname
$ ../obj/ppm show -p=newpwdo.ppm
Password mynewname
$ ../obj/ppm show -p=newpwdo.ppm . .
Password mynewname
Context : ctx , Username : uname , Password : pwd
Context : hotmail , Username : myname , Password : pylorus10396Anole18213
Context : myname , Username :  , Password : spine64441Microbe54261

```



