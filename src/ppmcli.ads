with GNAT.Strings ;
package ppmcli is

   VERSION : string := "V01" ;
   NAME : String := "ppm" ;
   type commands is
     (
      create ,
      show , 
      set ,
      reset ,
      test ,
      help ) ;
   
   UNKNOWN_COMMAND : exception ;
   
   command : commands := help ;
   Verbose : aliased boolean ;
   HelpOption : aliased boolean ;
   
   forceOption : aliased boolean := false ;
   generateOption : aliased boolean := false ;
   keepOption : aliased boolean := false ;
   PwdFile : aliased GNAT.Strings.String_Access ;
   wordlistFile : aliased GNAT.Strings.String_Access ;
   NumSegments : aliased integer ;
   MaxWordLength : aliased integer ;
   separator : aliased GNAT.strings.string_access;
   overrideOption : aliased boolean ;
   builtinOption : aliased boolean ;
   
   clipOption : aliased boolean := false ;
   
   procedure ProcessCommandLine ;
   procedure ShowCommandLineOptions ;
   function GetNextArgument return String ;
   
   function Query( prompt : string ) return String ;
end ppmcli;
