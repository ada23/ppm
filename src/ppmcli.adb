with Ada.Text_IO ; use Ada.Text_IO ;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io ;

with GNAT.Command_Line ;
with GNAT.Strings ;

package body ppmcli is

   procedure SwitchHandler
     (Switch : String; Parameter : String; Section : String)
   is
   begin
      Put ("SwitchHandler " & Switch);
      Put (" Parameter " & Parameter);
      Put (" Section " & Section);
      New_Line;
   end SwitchHandler;

   
   procedure ProcessCommandLine is
      Config : GNAT.Command_Line.Command_Line_Configuration;   
   begin
      GNAT.Command_Line.Set_Usage( config ,
                                   "subcommands: create|set|show|reset. use --help with the subcommand" 
                                   );
      GNAT.Command_Line.Initialize_Option_Scan(Stop_At_First_Non_Switch => true );
      GNAT.Command_Line.Define_Switch
        (Config, Verbose'access, Switch => "-v" , Long_Switch => "--verbose",
                   Help                           => "be verbose");      
      GNAT.Command_Line.Define_Switch
        (Config, PwdFile'access, Switch => "-p=", Long_Switch => "--password-file=",
         Help                           => "Password File");
      GNAT.Command_Line.Define_Switch
        (Config, keepOption'access, Switch => "-k", Long_Switch => "--keep",
                   Help                           => "keep intermediate files");
      
      GNAT.Command_Line.Getopt (Config, SwitchHandler'access);
      declare
         subcommand : string := GNAT.Command_Line.Get_Argument (Do_Expansion => False);
      begin
         if subcommand = "test"
         then
            command := test ;
         elsif subcommand = "create"
         then
            GNAT.Command_Line.Set_Usage( config ,
                                         "create a new personal password database"
                                              );
            
            command := create ;
            GNAT.Command_Line.Define_Switch
            (Config, forceOption'access, Switch => "-f", Long_Switch => "--force",
                     Help                           => "force the creation. erase existing file");
         elsif subcommand = "set"
         then
            GNAT.Command_Line.Set_Usage( config ,
                                         "set the credentials for a new context(site)"
                                                    );
            
            command := set ;

            GNAT.Command_Line.Define_Switch
              (Config, WordListFile'access, Switch => "-f=",
               Long_Switch => "--word-list-file-name=", Help => "Word list file name");
            GNAT.Command_Line.Define_Switch
              (Config, Separator'access, Switch => "-t=",
               Long_Switch => "--separator=", Help => "Separator");            
            GNAT.Command_Line.Define_Switch
              (Config, generateOption'access, Switch => "-g", Long_Switch => "--generate",
               Help                           => "generate a password");
            GNAT.Command_Line.Define_Switch
              (Config, builtinOption'access, Switch => "-b", Long_Switch => "--builtin-wordlist",
               Help                           => "use builtin wordlist for generation");

            GNAT.Command_Line.Define_Switch
              (Config, overrideOption'access, Switch => "-o", Long_Switch => "--override",
               Help                           => "override the existing password");
            GNAT.Command_Line.Define_Switch
              (Config, NumSegments'access, Switch => "-s=", Long_Switch => "--segments=",
               Initial => 2 , Default => 2 ,
               Help                           => "Number of Segments");
            GNAT.Command_Line.Define_Switch
              (Config, MaxWordLength'access, Switch => "-m=", Long_Switch => "--max-word-length=",
               Initial => 8 , Default => 6 ,
               Help                           => "Maximum length of words");

         elsif subcommand = "reset"
         then
            command := reset ;
            GNAT.Command_Line.Set_Usage( config ,
                                        "reset the credentials db. create a new one with a different password" );

             GNAT.Command_Line.Define_Switch
            (Config, forceOption'access, Switch => "-f", Long_Switch => "--force",
                     Help                           => "force the creation. erase existing file");

         elsif subcommand = "show"
         then
            GNAT.Command_Line.Set_Usage( config ,
                                         "show the credentials. use . as a wildcard for all"
                                                    );
            command := show ;
            GNAT.Command_Line.Define_Switch
              (Config, clipOption'access, Switch => "-c", Long_Switch => "--clip",
               Help                           => "clip the password to the clipboard");
                
         else
            Put_Line("Unknown command");
            raise UNKNOWN_COMMAND ;
            return ;
         end if ;
      end ;
      
      GNAT.Command_Line.Initialize_Option_Scan(Stop_At_First_Non_Switch => false );
      GNAT.Command_Line.Getopt (Config, SwitchHandler'access);
      
      if Verbose
      then
         ShowCommandLineOptions ;
      end if ;   
   end ProcessCommandLine ;

   function GetNextArgument return String is
   begin
      return GNAT.Command_Line.Get_Argument (Do_Expansion => false );
   end GetNextArgument;

   function Query( prompt : string ) return String is
      response : String (1..64);
      responselen : natural;
   begin
      Put(prompt) ; Flush ; 
      Get_Line(response,responselen);
      return response(1..responselen);
   end Query ;
   
   package boolean_text_io is new Enumeration_IO (Boolean);
   use boolean_text_io;
   package commands_text_io is new Enumeration_IO(Commands);
   use commands_text_io ;
   procedure ShowCommandLineOptions is
      use GNAT.Strings ;
      demark : string := "===============================================" ;
   begin
      Put_Line(demark);
      Put ("Verbose "); Put (Verbose); New_Line;
      Put("Command ") ; put(command); New_Line ;
      if pwdfile /= null
      then
         Put("Password File :"); Put(PwdFile.all); New_Line;
      else
         Put_Line("No password file provided");
      end if ;
      Put("Force "); Put(forceOption); New_Line ;
      Put("Generate "); Put(generateOption); New_Line ;
      Put("Keep "); Put(keepOption); New_Line ;
      Put("Clip "); Put(clipOption); New_Line;
      if wordlistFile /= null
      then 
         Put("Wordlist file "); Put(wordlistFile.all); New_Line;
      else
         Put_Line("No wordlist file specified");
      end if ;
      if separator /= null
      then 
         Put("Separator "); Put(separator.all); New_Line ;
      else
         Put_Line("No separator provided");
      end if ;
      Put("Max Word Length "); Put(MaxWordLength); New_Line ;
      Put("Segments "); Put(NumSegments); New_Line;
      Put("Override "); put(overrideOption); new_line ;
      Put_Line(Demark);
   end ShowCommandLineOptions ;
   
end ppmcli;
