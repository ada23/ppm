with Ada.Text_Io; use Ada.Text_IO ;
with ada.Directories ;
with GNAT.OS_Lib ;

with sf.creator ;
with sf.reader ;
with numbers ;

package body spf.reset is

   procedure Exec( PwdFile : string ;
                   password : aliased string ;
                   newname : string ;
                   newpassword : aliased string ;
                   forceOption : boolean ;
                  keepOption : boolean ) is
      spwdf : sf.SecureFile_Type ;
      spwdfo : sf.SecureFile_Type ;
      
      pwdfn : string := "PPMRESET" & numbers.Generate & ".tmp" ;
      
   begin
      if GNAT.OS_Lib.Is_Regular_File(newname)
      then
         if not forceOption
         then
            if Verbose
            then
               Put_Line("Existing file. raising exception");
            end if ;
            raise EXISTING_FILE ;
         end if ;
      end if ;
      
      sf.reader.Open(spwdf, pwdfile , password );
      sf.reader.Copy(spwdf, pwdfn) ; 
      sf.reader.Close(spwdf) ;
      sf.creator.Create(spwdfo , newname , newpassword ) ;
      sf.creator.Copy( spwdfo , pwdfn ) ;
      sf.creator.Close(spwdfo) ;
      if not keepOption
      then
         Ada.Directories.Delete_File (pwdfn);
      end if ;
   end Exec ;
  
end spf.reset;
