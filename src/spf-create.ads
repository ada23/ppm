package spf.create is

   procedure Exec( filename : string ;
                     forceOption : boolean := false ;
                     password : aliased string ) ;

end spf.create;
