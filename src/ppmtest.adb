with ppmcli ;
with spf ;
with passwords ;
with sf.creator ;
with sf.reader ;

procedure Ppmtest is
   db : spf.Pwd_Pkg.Map ;
   mypwds : sf.SecureFile_Type ;
   mypwd : aliased string := "srinivasan" ;
begin
   --  Insert code here.
   ppmcli.ProcessCommandLine ;
   db := spf.Load("pwd.db");
   declare
      newpwd : string := passwords.Generate("../../pwdgen/etc/physiology.txt");
   begin
      begin
         spf.SetPassword( db , "reddit.com" , "rajasrinivasan" , "extend" ) ;
      exception
         when others => null ;
      end ;

      spf.SetPassword( db , "reddit.com" , "rs@toprllc.com" , newpwd ,override => true ) ;
      spf.Save( db , "pwd.db.new" );
   end ;
   sf.creator.Create(mypwds,"mypwds.ppm" , mypwd);
   sf.creator.copy( mypwds , "pwd.db.new" );
   sf.creator.close( mypwds ) ;

   sf.reader.Open(mypwds,"mypwds.ppm",mypwd) ;
   sf.reader.Copy(mypwds,"pwd.db.ext");
   sf.reader.Close(mypwds);

end Ppmtest ;
