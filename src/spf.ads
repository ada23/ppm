with ada.strings.unbounded ; use ada.strings.unbounded ;
with ada.Containers.Ordered_Maps ;

package spf is
   
   verbose : boolean := false ; 
   EXISTING_FILE : exception ;
   UNKNOWN_CREDENTIAL : exception ;
   EXISTING_CREDENTIAL : exception ;
   SEPARATOR : constant string := ";" ;
   type PwdContext is
      record
         site : unbounded_string ;
         username : unbounded_string ;
      end record ;
   type Pwd is
      record
         password : unbounded_string ;
      end record ;
   
   function "<" (Left, Right : PwdContext) return Boolean ;
   function "=" (Left, Right : Pwd) return Boolean ;
   
   package Pwd_Pkg is new ada.containers.Ordered_Maps ( PwdContext , Pwd ) ;
   function  Load(filename : string;
                  remove : boolean := false ) return pwd_pkg.Map ;
   procedure SetPassword( pkg : in out Pwd_Pkg.Map ;
                  sitename : string ;
                 username : string ;
                 password : string ;
                 override : boolean := false ) ;
   function Get( pkg : Pwd_Pkg.Map ;
                 sitename : string ;
                 username : string ) return string ;
   procedure Save(pkg : pwd_pkg.Map ; filename : string);
   procedure Reveal( itemkey : PwdContext ; itemval : Pwd );
                                                      
end spf;
